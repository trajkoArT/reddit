import { Avatar, Button, Grid, List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, Paper, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import communityAvatar from "../assets/community.png";
import Header from "../components/header/Header";
import Post from '../components/post/Post';
import { NormalizedObjects } from '../store';
import { initLeaveCommunity } from '../store/community/action';
import { PostState } from '../store/post/types';
import { UiState } from '../store/ui/types';
import { UserState } from '../store/user/types';
import styles from "./css/userProfile.module.css";
import { fetchUserData } from '../store/user/action';
import { RouteComponentProps } from 'react-router';
import { CommunityState } from '../store/community/types';

interface propsFromState {
  ui: UiState,
  posts: NormalizedObjects<PostState>,
  users: NormalizedObjects<UserState>,
  communities: NormalizedObjects<CommunityState>
}

interface propsFromDispatch {
  initLeaveCommunity: typeof initLeaveCommunity,
  fetchUserData: typeof fetchUserData
}

type allProps = propsFromState & propsFromDispatch & RouteComponentProps<{ id: string }>;


class UserProfile extends Component<allProps> {

  componentDidMount() {
    this.props.fetchUserData();
  }

  render() {
    const user : UserState = this.props.users.byId[this.props.ui.loggedUser];
    
    return(
      <div>
        <Header isLoggedUser={this.props.ui.loggedUser !== 0}></Header>
        {this.renderUserPage(user)}
      </div>
    )
  }

  renderUserPage = (user: UserState) => {
    if(!this.props.users.isLoaded || this.props.ui.spinner || 
      !this.props.posts.isLoaded || this.props.ui.loggedUser === 0)
      return (<div></div>);

    return(
      <div className={styles.container}>
        <div className={styles.postsContainer}>
          {this.renderPosts(user)}
        </div>
        <div className={styles.communitiesContainer}>
          <div className={styles.communities}>
            <Paper elevation={3}>
            <Grid container spacing={2} className={styles.grid}>
                <Grid item xs>
                <Typography variant="h6" className={styles.title}>
                    {user.username + " communities"}
                </Typography>
                  {user.communities.length === 0 ? "Go 'All Communities' to join community" : ""}
                  {user.communities.map((community, index) =>
                    (<List key={index}>
                      <ListItem>
                        <ListItemAvatar>
                          <Avatar src={communityAvatar}></Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={this.props.communities.byId[community].title}/>
                        <ListItemSecondaryAction>
                          <Button 
                            onClick={() => this.props.initLeaveCommunity(community, user.id)}
                            color={"secondary"}
                            variant="outlined">
                              LEAVE
                          </Button>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </List>))
                  }
                </Grid>
              </Grid>
              </Paper>
            </div>
          </div>
        </div>);
  }

  renderPosts = (user: UserState) => {
    if(user.posts.length === 0) {
      return(
        <Typography className={styles.noPosts} variant="h6">Your didn't post anything!</Typography>
      );
    }
    else {
      return user.posts.map(post => {
        return (
          <Post key={post}
            isOpened={this.props.ui.isOpenedSinglePost}
            postState={this.props.posts.byId[post]}
            cardWidthInPercentage="90%">
          </Post>
        )
      })
    }
  }
}

const mapPropsToState = (reducer: any) => {
  return {
    ui: reducer.ui,
    posts: reducer.posts,
    users: reducer.users,
    communities: reducer.communities
  }
}

const mapPropsToDispatch = (dispatch: Dispatch) => {
  return {
    initLeaveCommunity: (community: number, user: number) => dispatch(initLeaveCommunity(community, user)),
    fetchUserData: () => dispatch(fetchUserData())
  }
}

export default connect(mapPropsToState, mapPropsToDispatch)(UserProfile);