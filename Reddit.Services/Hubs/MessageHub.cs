﻿using Microsoft.AspNetCore.SignalR;

namespace Reddit.Services.Hubs
{
    public class MessageHub : Hub
    {
        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }
    }
}
