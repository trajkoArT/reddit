﻿using Reddit.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reddit.Services.Repositories.Chat
{
    public interface IChatRepository : INeo4JRepository<Message>
    {
        Task<List<Message>> FindFrom(List<long> communities);
    }
}
