﻿namespace Reddit.Domain.Entities
{
    public class Message : IEntity
    {
        public long Id { get; set; }
        public long Sender { get; set; }
        public long Community { get; set; }
        public string Content { get; set; }
    }
}
