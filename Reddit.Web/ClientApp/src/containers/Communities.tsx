import { Avatar, Button, Grid, List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, Typography } from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Dispatch } from "redux";
import communityAvatar from "../assets/community.png";
import Header from "../components/header/Header";
import { NormalizedObjects } from "../store";
import { initJoinCommunity, initLeaveCommunity } from "../store/community/action";
import { CommunityState } from "../store/community/types";
import { fetchCommunityPosts } from "../store/post/action";
import { startSpinner } from "../store/ui/action";
import { UiState } from "../store/ui/types";
import { UserState } from "../store/user/types";
import styles from "./css/communities.module.css";

export const POPULAR_POSTS_CATEGORY = "Popular";
export const NEW_POSTS_CATEGORY = "New";
export const CONTROVERSIAL_POSTS_CATEGORY = "Controversial";

interface PropsFromState {
  ui: UiState,
  users: NormalizedObjects<UserState>,
  communities: NormalizedObjects<CommunityState>
}

interface PropsFromDispatch {
  fetchCommunityPosts: typeof fetchCommunityPosts,
  initJoinCommunity: typeof initJoinCommunity,
  initLeaveCommunity: typeof initLeaveCommunity,
  startSpinner: typeof startSpinner
}

interface IState {
  redirect: boolean,
  path: string
}

type allProps = PropsFromState & PropsFromDispatch;

class Communities extends Component<allProps, IState> {
  readonly state = {
    redirect: false,
    path: ""
  }

  render() {
    if (this.state.redirect && window.location.pathname !== this.state.path)
        return <Redirect push to={this.state.path} />

    return (
      <div>
        <Header isLoggedUser = {this.props.ui.loggedUser === 0 ? false : true}></Header>
        <div className={styles.communitiesContainer}>
          <div className={styles.communities}>
          <Grid container spacing={2}>
            <Grid item xs>
            <Typography variant="h6" className={styles.title}>
                All Communities
            </Typography>

              {this.props.communities.allIds.map((community, index) =>
                (<List key={index}>
                  <ListItem button onClick={() => this.openCommunity(this.props.communities.byId[community].title)}>
                    <ListItemAvatar>
                      <Avatar src={communityAvatar}></Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={this.props.communities.byId[community].title}/>
                    <ListItemSecondaryAction>
                      <Button 
                        onClick={() => this.leaveOrJoinCommunityButtonClick(community)}
                        color={this.alreadyJoined(community) ? "secondary" : "primary"}
                        variant="outlined">
                          {this.alreadyJoined(community) ? "LEAVE" : "JOIN"}
                      </Button>
                    </ListItemSecondaryAction>
                  </ListItem>
                </List>))
              }
            </Grid>
          </Grid>
        </div>
      </div>
      </div>
    );
  }

  alreadyJoined = (community: number) : boolean => {
    return this.props.users.byId[this.props.ui.loggedUser].communities.includes(community);
  }

  leaveOrJoinCommunityButtonClick = (community: number) => {
    this.props.startSpinner();
    if(this.props.users.byId[this.props.ui.loggedUser].communities.includes(community)) {
      this.props.initLeaveCommunity(community, this.props.ui.loggedUser);
    }
    else {
      this.props.initJoinCommunity(community, this.props.ui.loggedUser);
    }
  }

  openCommunity = (community: string) => {
    this.props.startSpinner();
    this.props.fetchCommunityPosts(community, POPULAR_POSTS_CATEGORY);
    this.setState({redirect: true, path: "/community/" + community});
  }
}

const mapStateToProps = (rootReducer: any) => {
  return {
    ui: rootReducer.ui,
    users: rootReducer.users,
    communities: rootReducer.communities
  }
}

const mapStateToDispatch = (dispatch : Dispatch) => {
  return {
    fetchCommunityPosts: (community: string) => dispatch(fetchCommunityPosts(community, POPULAR_POSTS_CATEGORY)),
    initJoinCommunity: (community: number, user: number) => dispatch(initJoinCommunity(community, user)),
    initLeaveCommunity: (community: number, user: number) => dispatch(initLeaveCommunity(community, user)),
    startSpinner: () => dispatch(startSpinner())
  }
}

export default connect(mapStateToProps, mapStateToDispatch)(Communities);