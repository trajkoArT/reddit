﻿using Reddit.Business.PostManagement.Input;
using Reddit.Domain.Interop;
using System.Threading.Tasks;

namespace Reddit.Business.PostManagement
{
    public interface IPostManager
    {
        Task<Result<long>> AddPost(CreatePostInput input);

        Task<Result<long>> AddCommentToPost(CreateCommentInput input);

        Task<Result> LikePost(long postId, long userId, string communityTitle);

        Task<Result> DislikePost(long postId, long userId, string communityTitle);


    }
}
