import { NormalizedObjects } from '..';
import { CommunityActionTypes, CommunityState, JoinCommunityAction, LeaveCommunityAction, LoadCommunitiesAction, InitLeaveCommunityAction, InitJoinCommunityAction } from './types';

export function leaveCommunity(community: number, user: number): LeaveCommunityAction {
  return {type: CommunityActionTypes.LEAVE_COMMUNITY, community: community, user: user}
}

export function joinCommunity(community: number, user: number): JoinCommunityAction {
  return {type: CommunityActionTypes.JOIN_COMMUNITY, community: community, user: user}
}

export function loadCommunities(communities: NormalizedObjects<CommunityState>): LoadCommunitiesAction {
  return { type: CommunityActionTypes.LOAD_COMMUNITIES, communities: communities }
}

export function initLeaveCommunity(community: number, user: number): InitLeaveCommunityAction {
  return {type: CommunityActionTypes.INIT_LEAVE_COMMUNITY, community: community, user: user}
}

export function initJoinCommunity(community: number, user: number): InitJoinCommunityAction {
  return {type: CommunityActionTypes.INIT_JOIN_COMMUNITY, community: community, user: user}
}